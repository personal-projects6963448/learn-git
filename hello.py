""""Этот модуль создан для тестирования пайплайнов gitlab"""


def hello(name):
    """Функция приветствия"""
    print(f"Hello, {name}")


def main():
    """Точка вызова программы"""
    hello("ProductStar")


if __name__ == "__main__":
    main()
